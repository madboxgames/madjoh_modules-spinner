# spinner #

v1.0.2

This module enables a loading spinner display.

## showLoader, hideLoader ##

#### Example ####
```js
// Spinner.count === 0
Spinner.show();
// Spinner.count === 1
Spinner.hide();
// Spinner.count === 0
```

- A call to showLoader increments the counter and sets the style of *cacheBlock* and *ajaxLoader* to `display : block;`.
- A call to hideLoader decrements the counter and sets the style of *cacheBlock* and *ajaxLoader* to `display : none;`.