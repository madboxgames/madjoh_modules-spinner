define(function(){
	var Spinner = {
		count : 0,

		init : function(settings){
			Spinner.settings = settings;
		},

		show : function(){
			if(Spinner.count === 0){
				Spinner.settings.setVisible(true);
			}

			Spinner.count++;
		},
		hide : function(){
			if(Spinner.count === 0) return;

			Spinner.count--;

			if(Spinner.count === 0){
				Spinner.settings.setVisible(false);
			}
		},

		onProgress : function(percent){
			if(Spinner.settings.onProgress) Spinner.settings.onProgress(percent);
		}
	};

	return Spinner;
});